set -e
set -x

NDK="${NDK:-$ANDROID_NDK_HOME}"
DESTBASE="${DESTBASE:-$PWD}"

if [ ! -x configure ]
then
  echo "Run this script from the GMP/MPFR base directory"
  exit 1
fi

if [ ! -d ${NDK} ]
then
  echo "Please download and install the NDK, then update the path in this script."
  echo "  http://developer.android.com/sdk/ndk/index.html"
  exit 1
fi

function ensure_toolchain {
  PATH="${NDK}/toolchains/llvm/prebuilt/linux-x86_64/bin:${PATH}"
}


function gmp_make_compile_install {
  local TAG=$1
  local CCTAG=$2
  local CCTAG2=$3
  local MPN_PATH=$4
  shift; shift; shift; shift

  echo "======= COMPILING FOR $TAG ======="
  CPPFLAGS='-DGMP_DECIMAL_POINT=\(\".\"\)' ./configure --prefix=$DESTBASE/$TAG --disable-static --enable-shared --enable-cxx --build=x86_64-pc-linux-gnu $@ MPN_PATH="$MPN_PATH" "CC=${CCTAG}-linux-android${CCTAG2}-clang" "CXX=${CCTAG}-linux-android${CCTAG2}-clang++"
  make -j8 V=1 2>&1 | tee android-$TAG.log
  make install
  pushd $DESTBASE/$TAG
  popd
  make distclean
}

function mpfr_make_compile_install {
  local TAG=$1
  local CCTAG=$2
  local CCTAG2=$3
  shift; shift; shift

  echo "======= COMPILING FOR $TAG ======="
  CPPFLAGS='-DMPFR_LCONV_DPTS=0' ./configure --prefix=$DESTBASE/$TAG --with-gmp-include=$DESTBASE/$TAG/include --with-gmp-lib=$DESTBASE/$TAG/lib --disable-static --enable-shared --enable-cxx --build=x86_64-pc-linux-gnu $@ "CC=${CCTAG}-linux-android${CCTAG2}-clang" "CXX=${CCTAG}-linux-android${CCTAG2}-clang++"
  make -j8 V=1 2>&1 | tee android-$TAG.log
  make install
  #pushd $DESTBASE/$TAG
  #rm -rf usr
  #popd
  make distclean
}
